# Adding repositories and installing the packages
We need to add repo files to `/etc/yum.repos.d` directory and paste content from the links into them. 

```conf
nano /etc/yum.repos.d/rsyslog.repo  
nano /etc/yum.repos.d/guardtime.repo  
```  

After that we install all the required packages using  

```conf
yum install -y *package-list*
```  
  
# Configuration

## 1. Create logfile named `/var/log/userlog`  

```conf 
touch /var/log/userlog  
```  

## 2. Configure KSI module for rsyslog  

Activate signing module in `/etc/rsyslog.conf` by adding following lines.  

```conf
authpriv.*  action(type="omfile" file="/var/log/userlog"
   sig.provider="ksi_ls12"
   sig.syncMode="async"
   sig.hashFunction="SHA2-256"
   sig.block.levelLimit="8"
   sig.block.timeLimit="0"
   sig.aggregator.url="ksi+http://tryout.guardtime.net:8080/gt-signingservice"
   sig.aggregator.user="actual_user"
   sig.aggregator.key="actual_pass"
   sig.aggregator.hmacAlg="SHA2-256"
   sig.keepTreeHashes="off"
   sig.keepRecordHashes="on"
   )
```   

## 3. SELinux policy

First we need to ensure that needed packages are installed.  

```conf 
yum install -y policycoreutils policycoreutils-python checkpolicy
```  

Then we create a SELinux policy by creating a file `/etc/selinux/rsyslog-8080.te` with following content:  

```conf
module rsyslog-8080 1.0;

require {
 type syslogd_t;
 type http_cache_port_t;
 class tcp_socket name_connect;
}
allow syslogd_t http_cache_port_t:tcp_socket name_connect;
```   

Compile module:

```conf 
checkmodule -M -m -o rsyslog-8080.mod rsyslog-8080.te
```   

Create package:

```conf
semodule_package -o rsyslog-8080.pp -m rsyslog-8080.mod
```

Install package:

```conf
semodule -i rsyslog-8080.pp
```  

## 4. Restart rsyslog  

```conf
service rsyslog restart
```   
  
# Testing  

## 1. Monitoring files

```conf
tail -f /var/log/messages  
tail -f /var/log/userlog  
```  

## 2. Log traffic generation to `/var/log/userlog`  

```conf
for num in {1..10}; do logger -p authpriv.info "This is a glorious log line $num"; done  
```  
  
## 3. Rotate `/var/log/userlog` and send HUP signal to rsyslog  

```conf
mkdir rotations
mv /var/log/userlog rotations
mv -r /var/log/userlog.logsig.parts rotations
pkill -HUP rsyslog
```   

## 4. Use ​`logksi​` tool to integrate the signature files into a single file  

```conf
cd rotations
logksi integrate userlog 
```

This outputs a single .logsig file  

## 5. Verify that the integrity of /var/log/userlog has not been compromised  
Using provided credentials and output of `logksi conf` command we can compose a configuration file which we need to export.  

```conf 
export KSI_CONF=/full_path/to/ksi_config
```  

After that we can verify the integrity of the rotated userlog file with following command:  

```conf  
logksi verify userlog userlog.logsig -d 
```  

## 6. Manipulate a subset of log lines  
First we need to extract lines 2,3 and 5-10 from userlog. It can be achieved with following command:  

```conf  
logksi extract userlog -r 2,3,5-10 
```  

To see the contents of extracted file:  

```conf  
cat userlog.excerpt  
```  

Verification:  

```conf 
logksi verify userlog.excerpt userlog.excerpt.logsig -d
```  

Modification: 

```conf  
nano userlog.excerpt 
```  

After modification verification fails, telling what log hash mismatched with the integrity proof file. After reverting the changes verification is again successful.