# Lab 1: Default rsyslog configuration
1) Check rsyslog version: `rsyslogd -v`
2) rsyslog.conf is located at `/etc/rsyslog.conf`
   rsyslog.d at `/etc/rsyslog.d` contains files with logging rules
3) `systemctl status rsyslog`
4) `su unexisting user` - creates log message
   ```
   tail /var/log/auth.log
   ```
   shows the failed su attempt  
   
# Lab 2: Prepare LR for relaying
 `sudo nano /etc/rsyslog.conf` - to uncomment module loading
 and port configuration. Restart the service afterwards.

## Testing connection 
### Testing connection from LR   
TCP:  
   ```conf
   logger --server localhost --port 514 --tcp "Hello listener"
   ```   

   message logged to /var/log/syslog  

   ```conf
   nc -zv localhost 514
   ```  

   *Connection to localhost 514 port [tcp/shell] succeeded!* - on local shell  

UDP:  
   ```conf
   logger --server localhost --port 514 --udp "Hello listener"
   ```  

   message logged to /var/log/syslog  

   ```conf 
   nc -uzv localhost 514
   ```   

   *Connection to localhost 514 port [udp/*] succeeded!* - on local shell

###  Testing connection from LC
TCP:  

   ```conf
   nc -zv 192.168.180.193 514
   ```  

 *Connection to 192.168.180.193 514 port [tcp/shell] succeeded!* - on local shell   

```conf
   logger --server 192.168.180.193 --port 514 --tcp "Hello from LC"
   ```   

   logged to /var/log/syslog on LR   

UDP:  

   ```conf
   nc -uzv 192.168.180.193 514
   ```   

   *Connection to 192.168.180.193 514 port [udp/*] succeeded!* - on local shell  

   ```conf
   logger --server 192.168.180.193 --port 514 --udp "Hello from LC"
   ```   

   logged to /var/log/syslog on LR  

## Format conversion on mgmt
In `/etc/rsyslog.d/20-ufw.conf`

Old format:
```conf
:msg,contains,"[UFW " /var/log/ufw.log
```  

Converted:  

```conf
if $msg contains "[UFW " then {
  action(type="omfile" file="/var/log/ufw.log")
}
```   
  
  
In `/etc/rsyslog.d/21-cloudinit.conf`  

Old format:  

```conf
:syslogtag, isequal, "[CLOUDINIT]" /var/log/cloud-init.log
& stop
```  

Converted:
```conf
if $syslogtag == "[CLOUDINIT]" then {
  action(type="omfile" file="/var/log/clound-init.log")
  stop
}
```  

In `/etc/rsyslog.d/50-default.conf`  

Old format:
```conf
auth,authpriv.*                 /var/log/auth.log
*.*;auth,authpriv.none          -/var/log/syslog
kern.*                          -/var/log/kern.log
mail.*                          -/var/log/mail.log
mail.err                        /var/log/mail.err
*.emerg                         :omusrmsg:*
```

New format:
```conf
auth,authpriv.* {
  action(type="omfile" file="/var/log/auth.log")
}
*.*;auth,authpriv.none {
  action(type="omfile" file="/var/log/syslog")
}
kern.* {
  action(type="omfile" file="/var/log/kern.log")
}
mail.* {
  action(type="omfile" file="/var/log/mail.log")
}

mail.err {
  action(type="omfile" file="/var/log/mail.err")
}
*.emerg {
  action(type="omusrmsg" users="*")
}
```

After modifying configuration files and saving changes I restarted the service and it accepted the configuration.  

# Lab 3: Prepare LC
We need to modify `/etc/rsyslog.d/50-default.conf' on managed node. Using old stile we add this(it should forward using tcp):
```conf
*.* @@192.168.180.193
```
New format:
```conf
*.* {
        action(type="omfwd" target="192.168.180.193" port="514" protocol="tcp")
}
```
And restart the rsyslog service.  

Result verification:  

`On managed node:`
```
logger -p user.err "Error occured"
```
`On both management and managed nodes:`
```
cat /var/log syslog
```
shows - `Nov 11 13:19:26 LAB18LNX nimda: Error occured`

# Adding second relay
We just need to add another forwarding rule to
`/etc/rsyslog.d/50-default.conf' as follows(this should forward using udp):
```conf
*.* @logserver.example.net
```

New format:
```conf
*.* {
        action(type="omfwd" target="logserver.example.net" port="515" protocol="udp")
}
```
After restarting the service everything works except for machine being unable to resolve address of the server.
# Lab 4: UDP listener on LC
We need to modify `/etc/rsyslog.conf' in order to activate udp listener.

Old format:
```conf
$ModLoad imudp
$UDPServerRun 514
```

New format:
```conf
module(load="imudp")
input(type="imudp" port="514")
```
# Lab 5: Forwarding EventLog events
First we need to download rsyslog agent from
https://www.rsyslog.com/windows-agent/windows-agent-download/. After installation we go to `License` and activate our license. Then we expand `RuleSets` tab all the way down to `Rsyslog`. There we configure ip address of our LC and port and specify protocol type as `UDP`. Then we restart the agent.

On `LC`:
```conf
tail -f /var/log/syslog
```
shows lots of logs coming from windows machine.# Lab 6: Forwarding windows logs without storaga
To only forward Windows logs we need a ruleset.
First we create a file in `rsyslog.d`:
```conf
sudo nano /etc/rsyslog.d/10-win.conf
```
With the following content:
```conf
ruleset(name="winfwd") {
    if $fromhost-ip == '192.168.180.148' then {
         action(type="omfwd" target="192.168.180.193" port="514")
         stop
    }
    action(type="omfwd" target="192.168.180.193" port="514")
    action(type="omfile" file="/var/log/syslog")
}
```
Now we need to bind this ruleset in `/etc/rsyslog.conf`.

Old style:
```conf
$InputUDPServerBindRuleset winfwd
```

New style:
```conf
input(type="imudp" port="514" ruleset="winfwd")
```

After modifying config we need to restart service.

Verification:
```conf
tail -f /var/log/syslog
```
on `both` linux systems. Only management node gets the messages.

# Lab 7: Monitoring file
We need to add the following lines to `/etc/rsyslog.conf`
Old format:
```conf
$ModLoad imfile
$InputFileBindRuleset winfwd
$InputFileName /var/log/mylog
$InputFilePollingInterval 10
$InputFileTag mytag
$InputRunFileMonitor
```
New format:
```conf
module(load="imfile" PollingInterval="10")
input(type="imfile" file="/var/log/mylog" tag="mytag" ruleset="winfwd")
```
  
`On managed node:`  
```conf
echo "incoming change" >> /var/log/mylog
```  

`On management node:`  
```conf
cat /var/log/syslog | grep mytag 
```  

shows - ```mytag incoming change```  

# Lab 8: Filtering messages
We need to add following statement to already existing ruleset(winfwd)
```conf
if $msg contains "junk" then stop
```
Verification:
We can use already file forwarding for checking.  

`On managed node:`  

```conf
 echo 'this message contains junk' >> /var/log/mylog
```  

`On management node:`

```conf
cat /var/log/syslog | grep mytag 
```  

doesn't show anything.
